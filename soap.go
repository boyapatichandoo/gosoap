package gosoap

import (
	"bytes"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"text/template"
)

//GetSetRequest parse the request template and adds passed request data to the parsed template and returns request bytes and err
func GetSetRequest(requestTemplate string, requestData interface{}) (io.Reader, error) {
	requestDataTemplate, err := template.New("InputRequest").Parse(requestTemplate)
	if err != nil {
		return nil, err
	}
	requestsBytes := &bytes.Buffer{}
	err = requestDataTemplate.Execute(requestsBytes, requestData)
	if err != nil {
		return nil, err
	}
	if strings.Contains(requestsBytes.String(), "<nil>") && !(strings.Contains(requestsBytes.String(), "<nil/>") || strings.Contains(requestsBytes.String(), "<nil></nil>")) {
		bodyString := strings.ReplaceAll(requestsBytes.String(), "<nil>", "<nil/>")
		requestsBytes = &bytes.Buffer{}
		requestsBytes.WriteString(bodyString)
	}
	return requestsBytes, nil
}

//MakeRequest perform the http request to the passed details and returns response body bytes and err
func MakeRequest(url string, soapAction string, requestBytes io.Reader) ([]byte, error) {
	req, err := http.NewRequest(http.MethodPost, url, requestBytes)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-type", "text/xml")
	req.Header.Set("SOAPAction", soapAction)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}
